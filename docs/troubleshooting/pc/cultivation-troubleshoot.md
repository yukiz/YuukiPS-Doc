---
sidebar_position: 3
--- 

# Cultivation

Known issue for Cultivation and how to solve them

## Game excecutable not set!

![img](./img/game-path-not-found.png)

This problem happens when you haven't set the game path, to fix it simply set your game path on the launcher settings

![img](./img/game-path-culti.png)

## System Error. Please try again later.

Make sure you have "**Toggle Encryption**" Disabled on the launcher settings, and unchecked "**Use HTTPS**". This error occurs usually on localhost server
