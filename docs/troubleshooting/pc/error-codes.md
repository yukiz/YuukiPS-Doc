---
sidebar_position: 1
---

# Error Codes

List of common and known error codes

## Connection Errors

- ### 500

    `EXPERIMENTAL_RealPassword`

    This error caused by server side when the server set the `EXPERIMENTAL_RealPassword = true` and you are running a client without a metadata patch (which no longer works in 3.1>)

- ### 502

    The server you are trying to connect doesn't exist or it's down. Check the <b>[Status](https://stats.uptimerobot.com/L8Nx6FL4V)</b> page. Or join the <b>[Discord Server](https://discord.gg/yuukips)</b> to check real-time status.

## Client Errors

- ### 4201

    `DISPATCH_GLOBAL_GET_TIMEOUT`

- ### 4202

    `DISPATCH_GLOBAL_GET_ERROR`

- ### 4203

    `DISPATCH_GLOBAL_PARSE_EXCEPTION`

- ### 4204

    `DISPATCH_GLOBAL_PARSE_INVALID`

- ### 4205

    `DISPATCH_GLOBAL_CONFIG_PARSE_FAIL`

- ### 4206

    `DISPATCH_REGION_GET_TIMEOUT`

    Server Configuration problem (Server Side)

- ### 4207

    `DISPATCH_REGION_GET_ERROR`

- ### 4208

    `DISPATCH_REGION_PARSE_EXCEPTION`

- ### 4209

    `DISPATCH_REGION_PARSE_INVALID`

- ### 4210

    `DISPATCH_CONFIG_PARSE_EXCEPTION`

- ### 4211

    `DISPATCH_GLOBAL_CONFIG_PARSE_INVALID`

- ### 4212

    `DISPATCH_REGION_RSP_INVALID`

- ### 4213

    `DISPATCH_REGION_ERR_WITH_CODE`

- ### 4214

    Patch Error. If you get this error on Official Server, do not run your proxy and revert the patch (UserAssembly, Metadata, RSA, etc.). If private server, do the opposite. Join our [Discord Server](https://discord.gg/yuukips) to download the client patch.

- ### 4301

    `LOGIN_TOKEN_GET_FAIL`

- ### 4302

    `LOGIN_PLAYER_LOGIN__FAIL`

- ### 4303

    `LOGIN_ENTER_SCENE_READY_FAIL`

- ### 4304

    `LOGIN_INIT_FINISH_FAIL`

- ### 4305

    `LOGIN_ENTER_SCENE_DONE_FAIL`

- ### 4306

    `LOGIN_POST_ENTER_SCENE_FAIL`

- ### 4307

    `LOGIN_ENTERTOKEN_INVALID`

- ### 4308

    `LOGIN_TASK_TIMEOUT`

:::info
If you get an error listed above and if there is no description for it or it isn't listed above, please submit a support forum in our <b>[Discord Server](https://discord.gg/yuukips)</b> and describe the issue.
:::
