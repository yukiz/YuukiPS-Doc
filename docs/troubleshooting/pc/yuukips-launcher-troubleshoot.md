---
sidebar_position: 2
--- 

# YuukiPS Launcher

Known Issue for YuukiPS Launcher and how to solve them

## When launching the app it instantly closed

This happens when you haven't installed [**.NET Runtime 6.0 (Desktop)**](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime). To fix this, simply install dotnet (Run desktop apps). Either the x64, x84 or arm64 depends on ur CPU architecture. [**Click here for how to check ur CPU architecture**](https://pcguide101.com/cpu/what-is-my-processor-architecture/)

## No game file config found

![img](./img/no-game-config.png)

This happens when your game version is not supported by the launcher (AKA you have newer version of the game or a client that is not supported by the MD5 API)

![img](./img/md5-error.png)

Check the console and send a screenshot of it to the support channel in [Discord](https://discord.gg/yuukips). <u>But please make sure to read before posting</u>, if someone already posted about it do not post it again just join the conversation, **use the search function**.
