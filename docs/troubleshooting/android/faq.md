---
sidebar_position: 1
title: "FAQ"
---

# FAQ

:::info
List of Frequently Asked Questions (FAQ) for Android.
:::

## Why stuck "Preparing to load data 45%"?

To fix this you must to have a data game first.<br/>
Go to `/sdcard/Android/data`<br/>
If you using V1 then rename it to `com.z3ro.YuukiPS`<br/>
If you using V2 then rename it to `com.miHoYo.GenshinImpact`

If still not working even you already rename it to `com.z3ro.YuukiPS` or `com.miHoYo.GenshinImpact` then try to download the Data Game from Official Server and rename it to `com.z3ro.YuukiPS` or `com.miHoYo.GenshinImpact` and try again.

<iframe width="420" height="215" src="https://www.youtube.com/embed/orOCazI4uVA" title="Tutorial" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## What's difference between Version 1 and 2?

Version 1 can connect to Localhost, YuukiPS, and other servers<br/>
Version 2 can connect to LocalHost, Official Server, YuukiPS and better for Fix Problem (0kb) loading time.

## What's difference between GIO and GC?

GIO is a clone of all functions from official server which means all functions work 100% without any bugs and quests all work but can only run on Version 3.2.

GC is a server emulator that can run on latest versions and beta versions, but many functions don't work, it's just that commands are easier because they can be accessed via Ayaka Chat.

## Failed to load il2cpp

Fix by delete folder "il2cpp" in Game Data Folder and enter game, the game opens without an error.

## Error 4214

If you enter official server, which actually is not allowed to connect to official server using apk launcher mod because it is risky

If you try to connect to Private Server and get this error message, then the proxy failed to change a domain or server to Private Server. You can try to re open the game and try again

## Android 13 rename

### ADB

You can try to rename the folder `com.miHoYo.GenshinImpact` to `com.z3ro.YuukiPS` with ADB<br/>

```bash
adb shell mv /sdcard/Android/data/com.miHoYo.GenshinImpact /sdcard/Android/data/com.z3ro.YuukiPS
```

![adb](./gif/adb.gif)

### Files

You can try to rename the folder `com.miHoYo.GenshinImpact` to `com.z3ro.YuukiPS` with Files App<br/>
Download [Files](https://play.google.com/store/apps/details?id=com.marc.files)

![files](./gif/files.gif)

### Still not working?

If you still can't rename the folder, it means your system is not allowed to rename the folder in `/sdcard/Android/data/`<br/>
Even with ADB or Files App, you can't rename the folder.<br/>
However the last solution is to root your phone for can rename the folder.<br/>
If you don't want to root your phone, then try to use V2 instead of V1. Check [How to install V2](/docs/tutorial-connect/android/install#download-from-official-server)

## Is Root Needed?

No, Root is not needed for this mod.

## Can I use VPN for this mod?

Yes, you can use VPN for this mod. This mod not using a proxy that need to use Proxy (127.0.0.1:PORT) to connect to Private Server.

## How to back to Official Server?

### YuukiPS V1

1. Go to `/sdcard/Android/data` and rename the folder `com.z3ro.YuukiPS` to `com.miHoYo.GenshinImpact`.
2. If you already uninstall the `Genshin Impact` then download it from Play Store
3. Open `Genshin Impact`

### YuukiPS V2

#### Install the No Module

1. you can download the [No Module](/docs/tutorial-connect/android/download#no-module)
2. And install the `Genshin Impact` for playing Official Server
3. If you want to play Private Server again, just download a [Mod APK](/docs/tutorial-connect/android/download#mod-apk) and install it for playing Private Server

#### Install from Play Store

1. You need to backup your game data first before playing Official Server
2. Go to `/sdcard/Android/data` and rename the folder `com.miHoYo.GenshinImpact` to `com.miHoYo.GenshinImpact.bak`
3. Then uninstall the `Genshin Impact`
4. And install the `Genshin Impact` from Play Store
5. Rename back the game data that you already backup `com.miHoYo.GenshinImpact.bak` to `com.miHoYo.GenshinImpact`

## Is it possible to play localhost on Android?

Yes, it is possible to play localhost on Android.
If you want to host Grasscutter on your PC and play it on your Android, you can check [DockerGS](https://github.com/YuukiPS/DockerGS)

If you want to host Grasscutter on your Android, you can check [GCAndroid](https://github.com/Score-Inc/GCAndroid)

After you host Grasscutter on your PC or Android, you can edit a file named "server.json" in Game Data Folder `/sdcard/Android/data/com.z3ro.YuukiPS/files` or `/sdcard/Download/YuukiPS`, and change the value of "server" to "127.0.0.1" or with your IP. Example : "server": "https://127.0.0.1:54321"<br/>
To get a PORT, you can check in `config.json` in Grasscutter folder after you run Grasscutter.

```json filename="config.json" {3}
"http": {
  "bindAddress": "0.0.0.0",
  "bindPort": 54321,
  "accessAddress": "127.0.0.1",
  "accessPort": 0,
  "encryption": {
    "useEncryption": true,
    "useInRouting": true,
    "keystore": "./keystore.p12",
    "keystorePassword": "123456"
  }
},
```
