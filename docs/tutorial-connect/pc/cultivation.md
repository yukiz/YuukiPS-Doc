---
sidebar_position: 4
--- 

# Cultivation

Guide to use Cultivation for connecting to Private Server
:::info
From version 2.7 and up [Requires Patch](/docs/intro)
:::

## Downloading

1. Get the latest version from [here](https://github.com/Grasscutters/Cultivation/releases)

2. Download either the `.zip` or `.msi` (Recommended)
![img](./img/download-culti.png)

## Running

1. Run the installer if you downloaded the `.msi`, Extract the zip to a folder if you installed the `.zip`, after that run `Cultivation.exe` with administrator

2. If you see a security warning, click on Yes. If you did not trust the CA, the launcher proxy will not work. You can install the proxy certificate in the launcher settings "**Install Proxy Certificate**" (Unless you are using a separate proxy daemon, you can set this in the launcher settings by toggling off "**Use Internal Proxy**")

    ![img](./img/trust-cert-culti.png)

3. Set the game path on the launcher settings (Locate `GenshinImpact.exe`)
![img](./img/game-path-culti.png)

4. Toggle "**Connect via Grasscutter**" and "**use HTTPS**\*", then enter server address `ps.yuuki.me` and port number (443 is the default port for grasscutter), after that click on Launch

    *Disable Only if there's no SSL Certificate installed on the domain

    ![img](./img/launch-game-culti.png)

    Allow firewall access

    ![img](./img/allow-firewall-culti.png)

5. Wait until the game launch
