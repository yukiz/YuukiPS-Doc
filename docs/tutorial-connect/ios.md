---
sidebar_position: 3
---

# iOS

# 0.Overview

This project supports resource hot downloading for 3.6 iOS clients and game-in-game downloading resources, without replacing the software.

# 1.Download resources

iOS3.6：[mihoyu.cn](https://oss.mihoyu.cn/d/IOS/yuanshenGC3.6.0.ipa)

Shadowrocket：[GD](https://file2.yuuki.me/GD1/Project/GenshinImpact/App/Shadowrocket.ipa)

Sign it and install it by yourself.

# 2.Getting started

## 2.1 Local proxy method

Conditions: Only an iOS device with a proxy software installed is required, no PC is needed.

Restrictions: Only supports servers with dispatch as the https protocol and port 443.

Here is an example using Shadowrocket, with Surge/Loon being similar.

### 2.1.1 Turn on https decryption

0.If it has been opened, you can skip this step.

1.Open Shadowrocket, click the bottom configuration menu item, open the arrow-indicated i icon on the right.

2.Click on https decryption, turn on https decryption, and click on the √ sign above.

3.Click Generate New CA Certificate, wait for a moment, and then there will be a pop-up, click confirm.

4.Click Install Certificate, and click Allow.

5.Return to the desktop, open Settings - Downloaded Profiles, and install.

6.Not done yet! Open Settings again - General - About this Mac - Trusted Certificate Repositories - select the certificate that was just generated.

### 2.1.2 Add module

1.Open Shadowrocket, click bottom configuration menu item, open modules, and select New module.

2.Paste the following code, click save.

```RE
!name=代理模块
[URL Rewrite]
^https:\/\/([\da-z-.]+).hoyoverse.com https://ps.yuuki.me header
^https:\/\/([\da-z-.]+).mihoyo.com https://ps.yuuki.me header
^https:\/\/hoyoverse.com https://ps.yuuki.me header
^https:\/\/mihoyo.com https://ps.yuuki.me header
[MITM]
hostname = %APPEND% .hoyoverse.com,.mihoyo.com
```

3.Click on the settings, select UDP, and turn on the "forwarding" option.

### 2.1.3 Downloading Resources

Turn on the proxy, enter game, enter any account information, and download resources and play it

