---
sidebar_position: 2
description: How to install and connect to Private Server on Android.
---

# Install

:::tip
Choose one of the following methods to install the game, and don't forget to download the game data first.
:::

## Install Mod Launcher only (No-Root) (3.2 GIO - 4.0 GC - 4.5 GC)

- Download [Mod Launcher 3.2, 4.0 or 4.5](./download-genshin-impact).
- Install Mod Launcher (you can also install the original if you want both).
- Open the game and download the game data (in-game) to play it.

## Copy Data Game (No-Root) (3.2 GIO) (Not Recommended)

:::info
It's the same method, but you only need to download game data from us. Therefore, you don't need to download from the official server or use the Official Launcher at all.
:::

:::note
Running version 3.2 and playing with the Official Server (version 4.6) using the same Mod Launcher won't work due to different versions. Therefore, you have to separate them using Version 1, so you can use a different private server package name with the official server.
:::

- Download [Mod APK Launcher 3.2 V2](./download-genshin-impact)
- Download [Game Data 3.2](https://teraboxapp.com/s/1fSASCllYIQUmsIy9hZC04w) (make sure you have downloaded all parts)
- Download and install [ZArchiver][ZArchiver_Playstore].
- Open **ZArchiver** and unzip file `com.miHoYo.ys.x.zip`. You can navigate/copy directly to Game Data in `storage/emulated/0/Android/data`.
- Rename `com.miHoYo.ys.x` to `com.moe.yuukips32` (\*Android 13 needs root; others 11-12 don't need to root)
- Open Game **YuukiPS 3.2** to play on the private server.

Here are YouTube tutorials by Skill Issue how to play on a GIO:

<iframe width="420" height="215" src="https://www.youtube.com/embed/VHMor4bl_Cs" title="Genshin Impact Private Server Tutorial 1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<details>

<summary>Archives only/ not supported</summary>

## Copy Lib (Root) (3.6 GC)

:::info
This method uses the copy library method, so you need to root your device first.
:::

:::warning
If you have not installed LSPosed yet, you need to install it first. You can download it from [here](https://github.com/LSPosed/LSPosed/releases) and flash it with [Magisk](https://github.com/topjohnwu/Magisk)
:::

- Download [libil2cpp.so].
- Download [Anime Game][Anime_Game_3.6] (not from Play Store).
- Download [Module][Module_YuukiPS] and install it.
- Backup the `com.miHoYo.GenshinImpact` folder from `/storage/emulated/0/Android/data` to `com.miHoYo.GenShinImpact.backup`.
- Uninstall `Anime Game` from your device.
- Install `Anime Game` from the link above.
- Rename `com.miHoYo.GenShinImpact.backup` back to `com.miHoYo.GenshinImpact` from `/storage/emulated/0/Android/data`.
- Open a file manager, such as [MT Manager](https://apkcombo.com/mt-manager/bin.mt.plus/).
- Go to where you downloaded [Anime Game][Anime_Game_3.6], and hold it, then click on `APK path`.
  ![Click Apk Path](img/Root/Click_Apk_Path.png)
- Now it should take you to where [Anime Game][Anime_Game_3.6] is located at `/data/app`.
- Go to `lib/arm64` and copy the `libil2cpp.so` to another folder, such as the `Download` or `Documents` folder.
- Go to where you downloaded [libil2cpp.so] and copy it to `/data/app` that you previously opened by clicking `APK path`.
- Now open `LSPosed`, then press the `Module` button next to the download logo on the far left.
- Click on `YuukiPS` and enable it. Then use the search function to find `Genshin Impact` and enable it. If you can't find it, click on the top right -> `Hide` -> uncheck `Games`.
- Open `Genshin Impact`. If you see a menu, quickly click on the `CONTINUE` button.
  ![CONTINUE BUTTON ISSUE](img/Root/in_game.png)
- Done, you can now play on the private server.

:::tip
If you want to play on the official server, simply replace the `libil2cpp.so` file with the one you backed up earlier before replacing it with the [libil2cpp.so] mod.
:::

## Rename Folder/Download from Official APK (No-Root) (3.4 GC) V2

:::info
This method uses either a renamed folder package name or game data downloaded from the official server. If you don't have the game data, you can obtain it from the official APK launcher.
:::

### Rename Folder

- [Download Mod APK Launcher 3.4](./download#mod-apk) (Don't install yet)
- Make sure to back up your game data from the official APK launcher.
- Go to `/storage/emulated/0/Android/data` and rename `com.miHoYo.GenshinImpact` to `com.miHoYo.GenshinImpact.backup`. Note that on Android 13, you'll need to have root access, but on versions 11-12, you won't need root.
- After that, uninstall the official APK launcher and install the modded APK.
- Rename `com.miHoYo.GenshinImpact.backup` back to `com.miHoYo.GenshinImpact`. Note that on Android 13, you'll need to have root access, but on versions 11-12, you won't need root.
- Open the game **Genshin Impact** to play on the private server.

### Download from Official Server

- Download [Launcher Official 3.4](./download#launcher-official-download-via-a-third-party).
- If you've already installed the official APK, you need to delete it first.
- Install **Genshin Impact**.
- Log in with your dummy account.
- Wait until all game data is downloaded.
- After that, download and install [Mod APK Launcher 3.4](./download#mod-apk).
- Open the game **Genshin Impact** to play on the private server.

:::note
If you want to play on the official server, just install the [Launcher Official 3.4](./download#launcher-official-download-via-a-third-party) and log in.
:::

:::tip
The signature of the official APK and modded APK is the same, so you can overwrite the official APK with the modded APK without uninstalling it first.
:::

## Clone APK (No-Root) (3.4 GC) V1

:::info
This method uses the clone package name, so you need game data from the Official APK Launcher.
:::

- [Download Mod APK Launcher 3.4](./download#mod-apk-launcher-34-gc)
- Install **GI YuukiPS Z**
- Download and install [ZArchiver][ZArchiver_Playstore].
- Open **ZArchiver** and find the game data folder at `storage/emulated/0/Android/data`, then look for the folder with the name `com.miHoYo.GenshinImpact` for the official version.
- Rename `com.miHoYo.GenshinImpact` to `com.z3ro.YuukiPS`.
- If you want to play on the official server, just rename it back to `com.miHoYo.GenshinImpact`.

## Rename Folder from Official APK (No-Root) (3.3 GC) V1

:::info
This method uses renaming of the folder package name, so you will need game data from the Official APK Launcher.
:::

1. [Download Mod APK Launcher 3.3](./download#mod-apk-launcher-33-gc) (Don't install yet).
2. Install the **Official APK** and make sure you download the game data first by playing for the first time on the official server. Don't forget to download the language audio pack if needed (in-game).
3. Download and install [Zarchiver][ZArchiver_Playstore].
4. Open **ZArchiver** and find the game data folder in `storage/emulated/0/Android/data`, and look for a folder named `com.miHoYo.GenshinImpact` for the official version.
5. Rename `com.miHoYo.GenshinImpact` to `com.miHoYo.GenshinImpact.backup`. (Note: Android 13 requires rooting, while others, 11-12, do not.)
6. Uninstall the Official APK and install the Mod APK.
7. Open **ZArchiver** and find the game data folder in `storage/emulated/0/Android/data`, and look for a folder named `com.miHoYo.GenshinImpact.backup` for the official version.
8. Rename `com.miHoYo.GenshinImpact.backup` to `com.miHoYo.GenshinImpact`. (Note: Android 13 requires rooting, while others, 11-12, do not.)
9. Open the game **Genshin Impact** to play on the private server.

## Clone APK (No-Root) (3.3 GC) V2

:::info
V2 allows you to play both the Official Server and Private Server using two different applications. However, you need to rename or copy the game data from the Official APK Launcher.
:::

- [Download Mod APK Launcher V2](./download#mod-apk-launcher-33-gc)
- Install **GI YuukiPS Z**.
- Download and install [ZArchiver][ZArchiver_Playstore].
- Open **ZArchiver** and find the game data folder in `storage/emulated/0/Android/data`, then look for the folder named `com.miHoYo.GenshinImpact` for the official version.
- Rename `com.miHoYo.GenshinImpact` to `com.miHoYo.YuukiPS` (\*Requires root access for Android 13, no root access needed for others 11-12) to play on the Private Server.
- If you want to play on the **Official Server**, simply rename `com.miHoYo.YuukiPS` back to `com.miHoYo.GenshinImpact`.
- Open **GI YuukiPS Z** to play on the Private Server.

</details>

<!-- Download Link to Play Store -->

[ZArchiver_Playstore]: https://play.google.com/store/apps/details?id=ru.zdevs.zarchiver

<!-- Download link for Rooted Device -->

[Anime_Game_3.6]: https://www.apkmirror.com/apk/cognosphere-pte-ltd/genshin-impact/
[libil2cpp.so]: https://anonfiles.com/Ieg6Pfb6zd/libil2cpp_so
[Module_YuukiPS]: https://anonfiles.com/s8p9Pfb9z0/yuuki_yuukips_apk
