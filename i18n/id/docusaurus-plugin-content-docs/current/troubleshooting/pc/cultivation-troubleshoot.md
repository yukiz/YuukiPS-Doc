---
sidebar_position: 3
--- 

# Cultivation

Masalah yang diketahui untuk Cultivation dan cara memperbaikinya

## Game excecutable not set!

![img](./img/game-path-not-found.png)

Masalah ini terjadi ketika Anda belum mengatur jalur game, untuk memperbaikinya cukup atur jalur game Anda pada pengaturan launcher

![img](./img/game-path-culti.png)

## System Error. Please try again later.

Pastikan Anda telah menonaktifkan "**Toggle Encryption**" pada pengaturan launcher, dan tidak dicentang "**Use HTTPS**". Kesalahan ini biasanya terjadi pada server localhost.
