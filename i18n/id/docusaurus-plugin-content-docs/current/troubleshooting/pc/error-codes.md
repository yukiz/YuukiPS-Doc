---
sidebar_position: 1
---

# Error Codes

Dafter kode kesalahan umum yang mungkin Anda temui saat menggunakan proxy.

## Connection Errors

- ### 500

    `EXPERIMENTAL_RealPassword`

    Kesalahan ini disebabkan oleh server saat server mengatur `EXPERIMENTAL_RealPassword = true` dan Anda menjalankan klien tanpa metadata patch (yang tidak lagi berfungsi di 3.1>)

- ### 502

    Server yang Anda coba hubungi tidak ada atau sedang down. Periksa halaman <b>[Status](https://stats.uptimerobot.com/L8Nx6FL4V)</b>. Atau bergabunglah dengan <b>[Discord Server](https://discord.gg/yuukips)</b> untuk memeriksa status secara real-time.

## Client Errors

- ### 4201

    `DISPATCH_GLOBAL_GET_TIMEOUT`

- ### 4202

    `DISPATCH_GLOBAL_GET_ERROR`

- ### 4203

    `DISPATCH_GLOBAL_PARSE_EXCEPTION`

- ### 4204

    `DISPATCH_GLOBAL_PARSE_INVALID`

- ### 4205

    `DISPATCH_GLOBAL_CONFIG_PARSE_FAIL`

- ### 4206

    `DISPATCH_REGION_GET_TIMEOUT`

    Masalah konfigurasi server (Server Side)

- ### 4207

    `DISPATCH_REGION_GET_ERROR`

- ### 4208

    `DISPATCH_REGION_PARSE_EXCEPTION`

- ### 4209

    `DISPATCH_REGION_PARSE_INVALID`

- ### 4210

    `DISPATCH_CONFIG_PARSE_EXCEPTION`

- ### 4211

    `DISPATCH_GLOBAL_CONFIG_PARSE_INVALID`

- ### 4212

    `DISPATCH_REGION_RSP_INVALID`

- ### 4213

    `DISPATCH_REGION_ERR_WITH_CODE`

- ### 4214

    Kesalahan Patch. Jika Anda mendapatkan kesalahan ini di Server Resmi, jangan jalankan proxy Anda dan kembalikan patch (UserAssembly, Metadata, RSA, dll.). Jika Private Server, lakukan sebaliknya. Bergabunglah dengan [Discord Server](https://discord.gg/yuukips) kami untuk mengunduh patch klien.

- ### 4301

    `LOGIN_TOKEN_GET_FAIL`

- ### 4302

    `LOGIN_PLAYER_LOGIN__FAIL`

- ### 4303

    `LOGIN_ENTER_SCENE_READY_FAIL`

- ### 4304

    `LOGIN_INIT_FINISH_FAIL`

- ### 4305

    `LOGIN_ENTER_SCENE_DONE_FAIL`

- ### 4306

    `LOGIN_POST_ENTER_SCENE_FAIL`

- ### 4307

    `LOGIN_ENTERTOKEN_INVALID`

- ### 4308

    `LOGIN_TASK_TIMEOUT`

:::info
Jika Anda mendapatkan kesalahan yang tercantum di atas dan jika tidak ada deskripsi untuk itu atau tidak tercantum di atas, silakan ajukan forum dukungan di <b>[Discord Server](https://discord.gg/yuukips)</b> kami dan jelaskan masalahnya.
:::
