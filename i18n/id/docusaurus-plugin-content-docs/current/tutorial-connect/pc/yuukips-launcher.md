---
sidebar_position: 2
--- 

# YuukiPS Launcher

Panduan untuk menggunakan YuukiPS Launcher untuk terhubung ke Private Server

## Mengapa YuukiPS Launcher?

Ini memiliki:

- Deteksi game otomatis (jika Anda memiliki launcher resmi).
- Unduh Metadata dan UserAssembly patch atau file asli berdasarkan versi game saat ini atau dengan memilih folder (RSA patch segera).
- Verifikasi Data Game dan Patch berdasarkan MD5 yang cocok dari server check API.
- Patches Langsung (mode offline) menggunakan kunci yang mendukung Metadata & UserAssembly.
- Auto-rollback ke versi asli, sehingga Anda dapat memainkan versi resmi tanpa mengubah namanya lagi.
- Dukungan proxy lokal, sehingga tidak perlu menjalankan daemon proxy lain.
- Dukungan daftar server.
- Cheat Multi (Acrepi, Minty, Bcrepi, Bkebi, Cotton, dan lainnya) (segera).
- Game lain seperti SR support (segera).
- Auto-update.

## Persyaratan

- [.NET Runtime Desktop (6.0)](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)

:::info
Versi game dari Epic Games tidak didukung
:::

## Panduan Video

- https://youtu.be/PgC9DGo8Np4 - Versi Yuuki (Tidak tersedia)
- https://youtu.be/eN_Ur5Obhzo - Versi TheScore (**Tersedia**)
- https://youtu.be/7yyN2qsMR_0 - Versi Kwelp (**Tersedia**)

## Unduhan

1. Dapatkan versi terbaru dari [sini](https://github.com/YuukiPS/Launcher-PC/releases).

2. Unduh `YuukiPS.zip`. Pastikan Anda mengunduh versi terbaru.

![img](./img/download-launcher.png)

## Menjalankan

1. Ekstrak file zip ke dalam folder.

2. Jalankan `YuukiPS.exe` dengan hak administrator. Jika aplikasi tidak berjalan, lihat [troubleshooting](/docs/troubleshooting/pc/yuukips-launcher-troubleshoot#when-launching-the-app-it-instantly-closed).

3. Atur jalur game pada tab konfigurasi, lalu klik Simpan.

    ![img](./img/game-path.png)

    :::note
    Jika Anda melihat "No game file config found", silakan lihat [troubleshooting](/docs/troubleshooting/pc/yuukips-launcher-troubleshoot#no-game-file-config-found).
    :::

4. Pilih server, lalu klik tombol "Launch".

    ![img](./img/launch-game.png)

5. Jika Anda melihat peringatan keamanan, klik Ya. Jika Anda tidak mempercayai CA, proxy launcher tidak akan berfungsi. (Jika Anda tidak menggunakan daemon proxy terpisah, Anda dapat mengatur ini di pengaturan launcher dengan mematikan opsi **Enable**.)

    ![img](./img/trust-cert.png)

    Dan izinkan akses firewall.

    ![img](./img/allow-firewall.png)

6. Tunggu hingga game dimulai.
