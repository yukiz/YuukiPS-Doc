---
title: Unduh Star Rail
sidebar_position: 1
description: Halaman unduhan ini untuk versi PC Star Rail
---

# Unduh

:::info
Halaman unduhan ini untuk versi PC.<br/>
Jika Anda tidak memiliki klien game atau terlalu lambat untuk diunduh dengan server resmi, Anda dapat mencoba ini.
:::

## Pembaruan Game (hdiff)

:::note
Memerlukan Data Game Lengkap dari versi sebelumnya, contoh: 1.1.0 memerlukan 1.0.5 terlebih dahulu.
:::

### 1.0.5 > 1.1.0 (OS)

[Official][Official_hdiff_1.0.5_1.1.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: EDBC4E431730DC2E3EA5B500D93051DD)

<!-- <details>

<summary>Archives only/ not supported</summary>

TODO

</details> -->

## Data Game Lengkap

### 1.1.0 (OS)

[Official][Official_full_1.1.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_1.1.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: d25f4eaf495c372f6d3eaec8c73382a5)<br/>

### 0.70.0 alias CBT2 (OS)

[Official but 404 link][Official_full_0.70.0] | [EU1][EU1_full_0.70.0] | SG1 | GD1-1 | [GD1-2][GD1_full_0.70.0] | GD2 | GD3 | OD1 | [Terabox][TR_full_0.70.0]<br/>
(MD5: 1AE45C59F2A287C5E6E073C6AA249E55)<br/>

<!-- <details>

<summary>Archives only/ not supported</summary>

TODO

</details> -->

<!-- Game Data 1.1.0 OS -->
<!-- Full -->

[Official_full_1.1.0]: https://autopatchos.starrails.com/client/download/20230527111343_8wBHfb9dmez1XooM/StarRail_1.1.0.zip
[GD1_full_1.1.0]: https://file2.yuuki.me/GD1/Project/StarRail/Data/PC/1.1.0/Global/StarRail_1.1.0.zip

<!-- hdiff -->

[Official_hdiff_1.0.5_1.1.0]: https://autopatchos.starrails.com/client/hkrpg_global/35/game_1.0.5_1.1.0_hdiff_9czqxj0PVA6wltFp.zip

<!-- Game Data 0.70.0 aka CBT2 OS -->
<!-- Full -->

[Official_full_0.70.0]: https://autopatchcn.bhsr.com/client/0517/StarRail_202205130516_qqqUGS2V4QQ4tNYE.zip
[GD1_full_0.70.0]: https://file2.yuuki.me/GD1/Project/StarRail/Data/PC/0.70.0/Global/StarRail_202205130516_q2hnl9E6ACCDC5wC.zip
[EU1_full_0.70.0]: https://file2.yuuki.me/Local_EU/Project/StarRail/Global/StarRail_202205130516_q2hnl9E6ACCDC5wC.zip
[TR_full_0.70.0]: https://teraboxapp.com/s/1lzfTXCRkpe3IvWkiiyAsvg
