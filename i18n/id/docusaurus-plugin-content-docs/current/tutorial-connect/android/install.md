---
sidebar_position: 2
description: How to install and connect to Private Server on Android.
---

# Install

:::tip
Pilih salah satu dari metode berikut untuk menginstal game, dan jangan lupa untuk mengunduh data game terlebih dahulu.
:::

## Instal Mod Launcher saja (No-Root) (3,2 GIO - 3,6 GC)

* Unduh [Mod Launcher 3.2 atau 3.6](./download).
* Instal Mod Launcher (Anda juga dapat menginstal yang asli jika ingin keduanya).
* Buka game dan unduh data game (di dalam game) untuk memainkannya.

## Copy Lib (Root) (3.6 GC)

:::info
Metode ini menggunakan metode pustaka salinan, sehingga Anda perlu melakukan root pada perangkat Anda terlebih dahulu.
:::

:::warning
Jika Anda belum menginstal LSPosed, Anda perlu menginstalnya terlebih dahulu. Anda dapat mengunduhnya dari [sini](https://github.com/LSPosed/LSPosed/releases) dan flash dengan [Magisk](https://github.com/topjohnwu/Magisk).
:::

* Unduh [libil2cpp.so].
* Unduh [Anime Game][Anime_Game_3.6] (tidak dari Play Store).
* Unduh [Modul][Module_YuukiPS] dan instal.
* Cadangkan folder `com.miHoYo.GenshinImpact` dari `/storage/emulated/0/Android/data` ke `com.miHoYo.GenShinImpact.backup`.
* Hapus `Anime Game` dari perangkat Anda.
* Instal `Anime Game` dari tautan di atas.
* Ubah nama `com.miHoYo.GenShinImpact.backup` kembali menjadi `com.miHoYo.GenshinImpact` dari `/storage/emulated/0/Android/data`.
* Buka manajer file, seperti [MT Manager](https://apkcombo.com/mt-manager/bin.mt.plus/).
* Pergi ke tempat Anda mengunduh [Anime Game][Anime_Game_3.6], dan tahan, lalu klik pada `APK path`.
![Click Apk Path](img/Root/Click_Apk_Path.png)
* Sekarang harus membawa Anda ke tempat [Anime Game][Anime_Game_3.6] berada di `/data/app`.
* Pergi ke `lib/arm64` dan salin `libil2cpp.so` ke folder lain, seperti folder `Download` atau `Documents`.
* Pergi ke tempat Anda mengunduh [libil2cpp.so] dan salin ke `/data/app` yang sebelumnya Anda buka dengan mengklik `APK path`.
* Sekarang buka `LSPosed`, lalu tekan tombol `Module` di sebelah logo unduhan di sebelah kiri.
* Klik pada `YuukiPS` dan aktifkan. Kemudian gunakan fungsi pencarian untuk menemukan `Genshin Impact` dan aktifkan. Jika Anda tidak dapat menemukannya, klik kanan atas -> `Hide` -> hilangkan centang `Games`.
* Buka `Genshin Impact`. Jika Anda melihat menu, klik cepat pada tombol `CONTINUE`.
![CONTINUE BUTTON ISSUE](img/Root/in_game.png)
* Selesai, sekarang Anda dapat bermain di Private Server.

:::tip
Jika Anda ingin bermain di server resmi, cukup ganti file `libil2cpp.so` dengan yang Anda cadangkan sebelumnya sebelum menggantinya dengan mod [libil2cpp.so].
:::

## Salin Data Game (No-Root) (3,2 GIO)

:::info
Ini adalah metode yang sama, tetapi Anda hanya perlu mengunduh data game dari kami. Oleh karena itu, Anda tidak perlu mengunduh dari server resmi atau menggunakan Launcher Resmi sama sekali.
:::

:::note
Menjalankan versi 3.2 dan bermain dengan Server Resmi (versi 3.7) menggunakan Mod Launcher yang sama tidak akan berhasil karena versi yang berbeda. Oleh karena itu, Anda harus memisahkannya menggunakan Versi 1, sehingga Anda dapat menggunakan nama paket Private Server yang berbeda dengan server resmi.
:::

* Unduh [Mod APK Launcher 3.2 V2](./download#mod-apk-launcher-32-gio).
* Unduh Data Game 3.2 (pastikan Anda telah mengunduh semua bagian).
* Unduh dan instal [ZArchiver][ZArchiver_Playstore].
* Buka **ZArchiver** dan ekstrak file `com.miHoYo.ys.x.zip`. Anda dapat menavigasi/menyalin langsung ke Data Game di `storage/emulated/0/Android/data`.
* Ubah nama `com.miHoYo.ys.x` menjadi `com.miHoYo.GenshinImpact` (*Android 13 memerlukan akses root; yang lain 11-12 tidak perlu akses root).
* Buka Game **Genshin Impact** untuk bermain di Private Server.

Berikut adalah tutorial YouTube oleh Skill Issue tentang cara bermain di GIO:

<iframe width="420" height="215" src="https://www.youtube.com/embed/VHMor4bl_Cs" title="Genshin Impact Private Server Tutorial 1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<details>

<summary>Archives only/ not supported</summary>

## Ubah Nama Folder/Unduh dari APK Resmi (No-Root) (3,4 GC) V2

:::info
Metode ini menggunakan nama folder yang diubah atau unduhan data game dari server resmi. Jika Anda tidak memiliki data game, Anda dapat mengunduhnya dari APK launcher resmi.
:::

### Ubah Nama Folder

* [Unduh Mod APK Launcher 3.4](./download#mod-apk) (Jangan instal terlebih dahulu)
* Pastikan untuk mencadangkan data game Anda dari APK launcher resmi.
* Buka `/storage/emulated/0/Android/data` dan ubah nama `com.miHoYo.GenshinImpact` menjadi `com.miHoYo.GenshinImpact.backup`. Perhatikan bahwa pada Android 13, Anda perlu memiliki akses root, tetapi pada versi 11-12, Anda tidak memerlukan akses root.
* Setelah itu, uninstall APK launcher resmi dan instal APK modded.
* Ubah nama `com.miHoYo.GenshinImpact.backup` kembali menjadi `com.miHoYo.GenshinImpact`. Perhatikan bahwa pada Android 13, Anda perlu memiliki akses root, tetapi pada versi 11-12, Anda tidak memerlukan akses root.
* Buka game **Genshin Impact** untuk bermain di Private Server.

### Unduh dari Server Resmi

* Unduh [Launcher Resmi 3.4](./download#launcher-official-download-via-a-third-party).
* Jika Anda sudah menginstal APK resmi, Anda perlu menghapusnya terlebih dahulu.
* Instal **Genshin Impact**.
* Masuk dengan akun dummy Anda.
* Tunggu hingga semua data game diunduh.
* Setelah itu, unduh dan instal [Mod APK Launcher 3.4](./download#mod-apk).
* Buka game **Genshin Impact** untuk bermain di Private Server.

:::note
Jika Anda ingin bermain di server resmi, cukup instal [Launcher Resmi 3.4](./download#launcher-official-download-via-a-third-party) dan masuk.
:::

:::tip
Tanda tangan APK resmi dan APK modded sama, sehingga Anda dapat menimpanya dengan APK resmi dengan APK modded tanpa menghapusnya terlebih dahulu.
:::

## Kloning APK (No-Root) (3,4 GC) V1

:::info
Metode ini menggunakan nama paket kloning, sehingga Anda memerlukan data game dari APK Launcher Resmi.
:::

* [Unduh Mod APK Launcher 3.4](./download#mod-apk-launcher-34-gc).
* Instal **GI YuukiPS Z**.
* Unduh dan instal [ZArchiver][ZArchiver_Playstore].
* Buka **ZArchiver** dan cari folder data game di `storage/emulated/0/Android/data`, lalu cari folder dengan nama `com.miHoYo.GenshinImpact` untuk versi resmi.
* Ubah nama `com.miHoYo.GenshinImpact` menjadi `com.z3ro.YuukiPS`.
* Jika Anda ingin bermain di server resmi, cukup ubah kembali nama paket menjadi `com.miHoYo.GenshinImpact`.

## Ubah Nama Folder dari APK Resmi (No-Root) (3,3 GC) V1

:::info
Metode ini menggunakan pengubahan nama paket folder, sehingga Anda memerlukan data game dari APK Launcher Resmi.
:::

1. [Unduh Mod APK Launcher 3.3](./download#mod-apk-launcher-33-gc) (Jangan instal terlebih dahulu).
2. Instal **APK Resmi** dan pastikan Anda mengunduh data game terlebih dahulu dengan bermain untuk pertama kalinya di server resmi. Jangan lupa untuk mengunduh paket audio bahasa jika diperlukan (di dalam game).
3. Unduh dan instal [ZArchiver][ZArchiver_Playstore].
4. Buka **ZArchiver** dan cari folder data game di `storage/emulated/0/Android/data`, lalu cari folder dengan nama `com.miHoYo.GenshinImpact` untuk versi resmi.
5. Ubah nama `com.miHoYo.GenshinImpact` menjadi `com.miHoYo.GenshinImpact.backup`. (Perhatikan bahwa pada Android 13, Anda perlu memiliki akses root, tetapi pada versi 11-12, Anda tidak memerlukan akses root.)
6. Uninstall APK Resmi dan instal APK Modded.
7. Buka **ZArchiver** dan cari folder data game di `storage/emulated/0/Android/data`, lalu cari folder dengan nama `com.miHoYo.GenshinImpact.backup` untuk versi resmi.
8. Ubah nama `com.miHoYo.GenshinImpact.backup` kembali menjadi `com.miHoYo.GenshinImpact`. (Perhatikan bahwa pada Android 13, Anda perlu memiliki akses root, tetapi pada versi 11-12, Anda tidak memerlukan akses root.)
9. Buka game **Genshin Impact** untuk bermain di Private Server.

## Kloning APK (No-Root) (3,3 GC) V2

:::info
V2 memungkinkan Anda untuk bermain di Server Resmi dan Private Server menggunakan dua aplikasi yang berbeda. Namun, Anda perlu mengubah nama atau menyalin data game dari APK Launcher Resmi.
:::

* [Unduh Mod APK Launcher V2](./download#mod-apk-launcher-33-gc).
* Instal **GI YuukiPS Z**.
* Unduh dan instal [ZArchiver][ZArchiver_Playstore].
* Buka **ZArchiver** dan cari folder data game di `storage/emulated/0/Android/data`, lalu cari folder dengan nama `com.miHoYo.GenshinImpact` untuk versi resmi.
* Ubah nama `com.miHoYo.GenshinImpact` menjadi `com.miHoYo.YuukiPS` (*Android 13 memerlukan akses root; yang lain 11-12 tidak perlu akses root) untuk bermain di Private Server.
* Jika Anda ingin bermain di **Server Resmi**, cukup ubah kembali nama `com.miHoYo.YuukiPS` menjadi `com.miHoYo.GenshinImpact`.
* Buka **GI YuukiPS Z** untuk bermain di Private Server.

</details>

<!-- Download Link to Play Store -->
[ZArchiver_Playstore]: https://play.google.com/store/apps/details?id=ru.zdevs.zarchiver

<!-- Download link for Rooted Device -->
[Anime_Game_3.6]: https://www.apkmirror.com/apk/cognosphere-pte-ltd/genshin-impact/

[libil2cpp.so]: https://anonfiles.com/Ieg6Pfb6zd/libil2cpp_so

[Module_YuukiPS]: https://anonfiles.com/s8p9Pfb9z0/yuuki_yuukips_apk
