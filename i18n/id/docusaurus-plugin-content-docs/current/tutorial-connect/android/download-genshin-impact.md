---
title: Download Genshin Impact
sidebar_position: 1
description: Halaman unduhan untuk versi android genshin impact
---

# Download

:::info
Halaman unduhan untuk APK, data game, dan data suara.
:::

## Mod APK Launcher 3.6 (GC)

### Launcher Version 1 (OS) (R1)

OS V1: [EU1][YuukiPS_3.6_V1_R1_OS_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | [TeraBox][YuukiPS_3.6_V1_R1_OS_TR] | [Mediafire][YuukiPS_3.6_V1_R1_OS_MF] | [Mega][YuukiPS_3.6_V1_R1_OS_MG] | [OD1][YuukiPS_3.6_V1_R1_OS_OD1]<br/>
MD5: DAEBA30DBA4DE9A678C55F754E332C83 (com.yuuki.osps36)

### Launcher Version 1 (CN) (3.6)

CN V1: [EU1][YuukiPS_3.6_V1_R1_CN_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | TeraBox | Mediafire | Mega | [OD1][YuukiPS_3.6_V1_R1_CN_OD1]<br/>
MD5: 735908CA45C2C5ADC09E4C24DFE97D04 (com.yuuki.ysps36)

### Launcher Version 2 (3.6)

SOON

## Mod APK Launcher 3.2 (GIO)

### Launcher Version 1 (OS)

[EU1][YuukiPS_3.2_V1_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | [TeraBox][YuukiPS_3.2_V1_TeraBox] | [Mediafire][YuukiPS_3.2_V1_Mediafire] | [Mega][YuukiPS_3.2_V1_Mega] | [OD1][YuukiPS_3.2_V1_OD1]<br/>
MD5: FA8728014408A3F79328171A9FBAE28B (com.moe.yuukips32)

### Launcher Version 2 (OS) (3.2)

SOON

:::note
Untuk dukungan dengan IP / alamat kustom, Anda dapat mencoba mengakses [mihoyu.cn](https://mihoyu.cn/?p=20) ini juga adalah mod apk yang kami gunakan.
:::

:::note
17/03/2023 - Tidak perlu lagi mengunduh data game secara manual, semuanya dapat dilakukan di dalam game.
:::

## Launcher Official

[3.6.0 CN][LC_CN_Official_3.6]<br/>
[3.5.0 CN][LC_CN_Official_3.5]<br/>
[3.4.0 CN][LC_CN_Official_3.4]<br/>
[2.8.0 CN][LC_CN_Official_2.8]<br/>
[2.7.0 CN][LC_CN_Official_2.7]<br/>
[1.6.0 CN][LC_CN_Official_1.6]<br/>
[1.0.0 CN][LC_CN_Official_1.0]<br/>

## Launcher Official (Download via a third party)

[3.6.0 OS][LC_OS_TP_3.6]<br/>
[3.5.0 OS][LC_OS_TP_3.5]<br/>
[3.4.0 OS][LC_OS_TP_3.4]<br/>

## Game Data 3.2 (GIO)

:::note
Bagian 4GB berarti Anda harus mengunduh semuanya (18GB), yang mencakup audio bahasa Inggris saja.
:::

Part1: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.2_z01] | GD2 | [GD3][GD3_3.2_z01] | [OD1][OD1_3.2_z01] | [Terabox][Terabox_3.2_z01]<br/>
(MD5: C6138A8CF8B9E220825C64A6A1FB1BD4)<br/>
Part2: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.2_z02] | GD2 | [GD3][GD3_3.2_z02] | [OD1][OD1_3.2_z02] | [Terabox][Terabox_3.2_z01]<br/>
(MD5: 1FF7B3AEB29C045B29E64D6B12412C01)<br/>
Part3: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.2_z03] | GD2 | [GD3][GD3_3.2_z03] | [OD1][OD1_3.2_z03] | [Terabox][Terabox_3.2_z01]<br/>
(MD5: 1848D9343C3BBC8CA7B3931392DCA0CC)<br/>
Part4: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.2_z04] | GD2 | [GD3][GD3_3.2_z04] | [OD1][OD1_3.2_z04] | [Terabox][Terabox_3.2_z01]<br/>
(MD5: 1AA48B9CBC359B321D7DDACCCFB8BEFC)<br/>
Part5: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.2_zip] | GD2 | [GD3][GD3_3.2_zip] | [OD1][OD1_3.2_zip] | [Terabox][Terabox_3.2_z01]<br/>
(MD5: 262F0906C7B0FD76EE92ECB0F3055505)<br/>
Japanese Voice: [Terabox][Terabox_Japanese_Voice_3.2]<br/>
(MD5: 209e60a52604119b5e44f27362ad552a)

:::note
Jika Anda menggunakan Versi Launcher 2, pastikan untuk mengubah nama folder `com.miHoYo.ys.x` menjadi `com.miHoYo.GenshinImpact`.
Ingat:
:::

Pastikan untuk memeriksa file dengan MD5 Checker Tool untuk memastikan bahwa nilainya sama dengan aslinya dan tidak rusak.

Kami menyarankan untuk mengekstrak file ini melalui PC dan kemudian menyalinnya ke `/Android/data` (ingat untuk memastikan itu penyimpanan internal, bukan di `sdcard`).

<details>

<summary>Archives only / not supported</summary>

## Game Data 3.5 (GC)

:::note
Bagian 4GB berarti Anda harus mengunduh semuanya (total 26,6GB), yang mencakup audio bahasa Inggris, Jepang, dan China.
:::

Part1: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_z01] | GD2 | GD3 | [OD1][OD1_3.5_z01] | Terabox<br/>
(MD5: 133FCB1AA577EB81CB3B2DC280063F67)<br/>
Part2: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_z02] | GD2 | GD3 | [OD1][OD1_3.5_z02] | Terabox<br/>
(MD5: 2A5BD687C18E7713449825BEC224CCDB)<br/>
Part3: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_z03] | GD2 | GD3 | [OD1][OD1_3.5_z03] | Terabox<br/>
(MD5: B045444A20AC22CFA955AD5FEBE2B0CA)<br/>
Part4: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_z04] | GD2 | GD3 | [OD1][OD1_3.5_z04] | Terabox<br/>
(MD5: 11D331C209F4373CCE417E32079B5267)<br/>
Part5: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_z05] | GD2 | GD3 | [OD1][OD1_3.5_z05] | Terabox<br/>
(MD5: 02244CB7F6849C7F6D62614A999D2DDB)<br/>
Part5: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_z06] | GD2 | GD3 | [OD1][OD1_3.5_z06] | Terabox<br/>
(MD5: A451F399077C877B9F3010D66F7FC8FE)<br/>
Part6: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.5_zip] | GD2 | GD3 | [OD1][OD1_3.5_zip] | Terabox<br/>
(MD5: 951527F85E70394B54B2349B9946DC48)

Japanese Voice: EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | [Terabox][Terabox_3.5_jp]<br/>
(MD5: 8F45DD3811A8A77AAB36110F34C7ADCC)

## Game Data 3.4 (GC)

:::note
Bagian 4GB berarti Anda harus mengunduh semuanya (total 23GB), yang mencakup audio bahasa Inggris dan Jepang.
:::

Part1: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.4_z01] | GD2 | GD3 | [OD1][OD1_3.4_z01] | Terabox<br/>
(MD5: 15A67E610EE1F8BE610D65549FFCC133)<br/>
Part2: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.4_z02] | GD2 | GD3 | [OD1][OD1_3.4_z02] | Terabox<br/>
(MD5: EB5B72C62702DF54FBB627AD7FD144CE)<br/>
Part3: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.4_z03] | GD2 | GD3 | [OD1][OD1_3.4_z03] | Terabox<br/>
(MD5: 9BB3091C283F8BAEB1797925BECF9671)<br/>
Part4: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.4_z04] | GD2 | GD3 | [OD1][OD1_3.4_z04] | Terabox<br/>
(MD5: 4D1087E1F1810E1B8F9631CB6E66052F)<br/>
Part5: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.4_z05] | GD2 | GD3 | [OD1][OD1_3.4_z05] | Terabox<br/>
(MD5: 4E58BDB5D893054165625360E20B5281)<br/>
Part6: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.4_zip] | GD2 | GD3 | [OD1][OD1_3.4_zip] | Terabox<br/>
(MD5: 0425405A75713C3194638A6601421743)<br/>

:::note
Jika Anda menggunakan Versi Launcher 1, silakan ubah nama folder menjadi `com.z3ro.YuukiPS`.
:::

## Game Data 3.3 (GC)

:::note
Bagian 4GB berarti Anda harus mengunduh semuanya (total 22GB), yang mencakup audio bahasa Inggris dan Jepang.
:::

Part1: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.3_z01] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 161B5B3F1776D2E9A8E4519B1F48A078)<br/>
Part2: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.3_z02] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: F9648E88377CF25222C4C1D5D3737FF4)<br/>
Part3: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.3_z03] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1C66F500768AE3F0CB635F06399C449D)<br/>
Part4: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.3_z04] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: C87B741A0AFFA69488CDDC6E2C421D65)<br/>
Part5: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.3_z05] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 8B0410B80E8641279E15DBF77E200070)<br/>
Part6: EU1 | SG1 | GD1-1 | [GD1-2][GD12_3.3_zip] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 8C053C363D336E88945EC46FE38516C1)<br/>

:::note
Ubah nama folder menjadi `com.miHoYo.YuukiPS` jika Anda menggunakan Versi Launcher 2.
:::

## Mod APK Launcher 3.5 (GC)

### Launcher Version 1 (OS) (R2)

OS V1: [EU1][YuukiPS_3.5_V1_R2_OS_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | [TeraBox][YuukiPS_3.5_V1_R2_OS_TR] | [Mediafire][YuukiPS_3.5_V1_R2_OS_MF] | [Mega][YuukiPS_3.5_V1_R2_OS_MG] | [OD1][YuukiPS_3.5_V1_R2_OS_OD1]<br/>
MD5: 51763555B14733B523E548F828ACEC3A (com.moe.yuukips)

### Launcher Version 1 (CN) (3.5)

CN V1: [EU1][YuukiPS_3.5_V1_CN_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | [TeraBox][YuukiPS_3.5_V1_CN_TR] | Mediafire | Mega | OD1<br/>
MD5: 6E5A61DA73535BD0F654FEAD321EDDE9 (com.miHoYo.Yuanshen)

### Launcher Version 2 (3.5)

SOON

## Mod APK Launcher 3.4 (GC)

### Launcher Version 1 (3.4)

[EU1][YuukiPS_3.4_V1_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | [GD4][YuukiPS_3.4_V1_Gdrive] | TeraBox | Mediafire | Mega | OD1

### Launcher Version 2 (3.4)

[EU1][YuukiPS_3.4_V2_EU1] | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | [GD4][YuukiPS_3.4_V2_Mod_APK_GDrive] | TeraBox | Mediafire | Mega | OD1

## Mod APK Launcher 3.3 (GC)

### Launcher Version 1 (3.3)

[EU1][YuukiPS_3.3_V1_EU1] | SG1 | GD1-1 | [GD1-2][YuukiPS_3.3_V1_GD1-2] | [GD2][YuukiPS_3.3_V1_GD2] | [GD3][YuukiPS_3.3_V1_GD3] | [TeraBox][YuukiPS_3.3_V1_TeraBox] | [Mediafire][YuukiPS_3.3_V1_Mediafire] | [Mega][YuukiPS_3.3_V1_Mega] | [OD1][YuukiPS_3.3_V1_OD1]

### Launcher Version 2 (3.3)

[EU1][YuukiPS_3.3_V2_EU1] | SG1 | GD1-1 | [GD1-2][YuukiPS_3.3_V2_GD1-2] | [GD2][YuukiPS_3.3_V2_GD2] | [GD3][YuukiPS_3.3_V2_GD3] | [TeraBox][YuukiPS_3.3_V2_TeraBox] | [Mediafire][YuukiPS_3.3_V2_Mediafire] | [Mega][YuukiPS_3.3_V2_Mega] | [OD1][YuukiPS_3.3_V2_OD1]
</details>

<!-- Launcher -->

<!-- Official -->
[LC_CN_Official_3.6]: https://autopatchcn.yuanshen.com/client_app/download/Android/20230403105245_M6iuu1yxzEjZAws9/mihoyo/yuanshen_3.6.0.apk
[LC_CN_Official_3.5]: https://autopatchcn.yuanshen.com/client_app/download/Android/20230220115021_vt91MmoiVQHMXe4g/mihoyo/yuanshen_3.5.0.apk
[LC_CN_Official_3.4]: https://autopatchcn.yuanshen.com/client_app/download/Android/20230109151120_vmaeMAkG2koYOJDo/ydbackup11/yuanshen_3.4.0.apk
[LC_CN_Official_2.8]: https://autopatchcn.yuanshen.com/client_app/download/Android/20220704153809_VQU8ZYPfctnoKBrB/ydbackup21/yuanshen_2.8.0.apk
[LC_CN_Official_2.7]: https://autopatchcn.yuanshen.com/client_app/download/Android/20220516191350_a8NUjZ1Vie2blUVC/mihoyo/yuanshen_2.7.0.apk
[LC_CN_Official_1.6]: https://autopatchcn.yuanshen.com/client_app/Android/20210609_2758cc87f80c2355/ydbackup17/yuanshen_1.6.0.apk
[LC_CN_Official_1.0]: https://autopatchcn.yuanshen.com/client_app/Android/20200928_75e9e7b1e7e3cd66/ydbackup15/yuanshen_1.0.0.apk

<!-- Official third party -->

[LC_OS_TP_3.6]: https://www.apkmirror.com/apk/cognosphere-pte-ltd/genshin-impact/genshin-impact-3-6-0_13833164_13951843-release/genshin-impact-3-6-0_13833164_13951843-2-android-apk-download/
[LC_OS_TP_3.5]: https://www.apkmirror.com/apk/cognosphere-pte-ltd/genshin-impact/genshin-impact-3-5-0_13082099_13156424-release/genshin-impact-3-5-0_13082099_13156424-2-android-apk-download/
[LC_OS_TP_3.4]: https://www.apkmirror.com/apk/cognosphere-pte-ltd/genshin-impact/genshin-impact-3-4-0_12498014_12591909-release/genshin-impact-3-4-0_12498014_12591909-2-android-apk-download/

<!-- 3.6 V1 R1 (OS) -->
[YuukiPS_3.6_V1_R1_OS_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.6.0/Global/YuukiPS.apk
[YuukiPS_3.6_V1_R1_OS_OD1]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.6.0/Global/YuukiPS.apk
[YuukiPS_3.6_V1_R1_OS_TR]: https://teraboxapp.com/s/10x-54P69OKjecoC13Hn-Ag
[YuukiPS_3.6_V1_R1_OS_MF]: https://www.mediafire.com/file/fh3pg8kudnc07mc/YuukiPS_36_V1_R1_OS.apk/file
[YuukiPS_3.6_V1_R1_OS_MG]: https://mega.nz/file/wpE3nTgK#fw2MXSuDYCaCZjwKXlyj0FYQ-kVPUdyU39Pc03eEPg0

<!-- 3.6 V1 R1 (OS) -->
[YuukiPS_3.6_V1_R1_CN_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.6.0/Global/YuukiPS.apk
[YuukiPS_3.6_V1_R1_CN_OD1]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.6.0/Global/YuukiPS.apk

<!-- 3.5 V1 R2 (OS) -->
[YuukiPS_3.5_V1_R2_OS_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.5.0/Global/YuukiPS.apk
[YuukiPS_3.5_V1_R2_OS_OD1]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/YuukiPS.apk
[YuukiPS_3.5_V1_R2_OS_TR]: https://teraboxapp.com/s/1hBQQFvPQwJBbF-CCio-WzA
[YuukiPS_3.5_V1_R2_OS_MF]: https://www.mediafire.com/file/6hci46swpgrva0i/YuukiPS_35_V1_R2.apk/file
[YuukiPS_3.5_V1_R2_OS_MG]: https://mega.nz/file/088l2D5A#RE1HpKa4E_XXpDt42Jk-iSWxjIfOK3bq08zF-Ip6tD8

<!-- 3.5 V1 (OS) -->

<!-- 3.5 V1 (CN) -->
[YuukiPS_3.5_V1_CN_EU1]: https://file2.yuuki.me/p/Public/Local_EU/Project/GenshinImpact/Data/Android/3.5.0/Chinese/YuukiPS.apk
[YuukiPS_3.5_V1_CN_TR]: https://teraboxapp.com/s/1mLTuJGveNurqkwY_e6Tvtg

<!-- 3.4 V1 -->
[YuukiPS_3.4_V1_Gdrive]: https://drive.google.com/file/d/1-BlUyQ4nIch-2Zjs4m4aUtIjkYwWbXMa/view?usp=drivesdk
[YuukiPS_3.4_V1_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.4.0/Global/YuukiPS.apk

<!-- 3.4 V2 -->
[YuukiPS_3.4_V2_Mod_APK_GDrive]: https://drive.google.com/file/d/1Rp2MkSagw8FuUqO42go4Zx-GoiNkLnZU/view?usp=drivesdk
[YuukiPS_3.4_V2_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.4.0/Global/YuukiPS_V2.apk

<!-- 3.3 V1 -->
[YuukiPS_3.3_V1_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk
[YuukiPS_3.3_V1_GD1-2]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk
[YuukiPS_3.3_V1_GD2]: https://file2.yuuki.me/GD2/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk
[YuukiPS_3.3_V1_GD3]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.3.0/YuukiPS.apk
[YuukiPS_3.3_V1_TeraBox]: https://terabox.com/s/1nfg2l2G_u_2kse3nSceliA
[YuukiPS_3.3_V1_Mediafire]: https://www.mediafire.com/file/yq1pwcietv581d3/YuukiPS.apk/file
[YuukiPS_3.3_V1_Mega]: https://mega.nz/file/k08nQLaA#_I_8jfxDGvrNSIp39Ed1E0NC6RehqKAfZDJcRoHmmkM
[YuukiPS_3.3_V1_OD1]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS.apk

<!-- 3.3 V2 -->
[YuukiPS_3.3_V2_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk
[YuukiPS_3.3_V2_GD1-2]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk
[YuukiPS_3.3_V2_GD2]: https://file2.yuuki.me/GD2/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk
[YuukiPS_3.3_V2_GD3]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.3.0/YuukiPS_V2.apk
[YuukiPS_3.3_V2_TeraBox]: https://terabox.com/s/1cE9SJVfrMB3qsqKFHoBSgQ
[YuukiPS_3.3_V2_Mediafire]: https://www.mediafire.com/file/98ipo58puxdngxv/YuukiPS_V2.apk/file
[YuukiPS_3.3_V2_Mega]: https://mega.nz/file/U4tVAIZK#fnoWQoy96PpJeS_FKnUPQdOnKTpE8JY2LIq7M8fsGng
[YuukiPS_3.3_V2_OD1]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.3.0/Global/YuukiPS_V2.apk

<!-- 3.2 V1 -->
[YuukiPS_3.2_V1_EU1]: https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS.apk
[YuukiPS_3.2_V1_TeraBox]: https://teraboxapp.com/s/1sA2JtMVdI-AW3a-eY4leYg
[YuukiPS_3.2_V1_Mediafire]: https://www.mediafire.com/file/nel0ehxtut6ood5/YuukiPS.apk/file
[YuukiPS_3.2_V1_Mega]: https://mega.nz/file/YhNCADLS#ZiDpd9urG-MITwNKkWC-CWkmJh3KDapCARkz30gYj5g
[YuukiPS_3.2_V1_OD1]: https://file2.yuuki.me/d/Public/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/YuukiPS.apk

<!-- 3.2 V2 -->

<!-- Game Data -->

<!-- 3.5 GC GD1-2 -->
[GD12_3.5_z01]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z01
[GD12_3.5_z02]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z02
[GD12_3.5_z03]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z03
[GD12_3.5_z04]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z04
[GD12_3.5_z05]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z05
[GD12_3.5_z06]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z06
[GD12_3.5_zip]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.zip

<!-- 3.5 OD1 -->
[OD1_3.5_z01]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z01
[OD1_3.5_z02]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z02
[OD1_3.5_z03]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z03
[OD1_3.5_z04]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z04
[OD1_3.5_z05]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z05
[OD1_3.5_z06]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.z06
[OD1_3.5_zip]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.5.0/Global/Part/com.moe.yuukips-35.zip

<!-- 3.5 Terabox -->
[Terabox_3.5_jp]: https://terabox.com/s/1CV_FTS2-jr7pA05uMRMQtg

<!-- 3.4 GC GD1-2 -->
[GD12_3.4_z01]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z01
[GD12_3.4_z02]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z02
[GD12_3.4_z03]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z03
[GD12_3.4_z04]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z04
[GD12_3.4_z05]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z05
[GD12_3.4_zip]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.zip

<!-- 3.4 OD1 -->
[OD1_3.4_z01]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z01
[OD1_3.4_z02]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z02
[OD1_3.4_z03]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z03
[OD1_3.4_z04]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z04
[OD1_3.4_z05]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.z05
[OD1_3.4_zip]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.4.0/Global/Part/com.miHoYo.GenshinImpact.zip

<!-- 3.3 GC GD1-2 -->
[GD12_3.3_z01]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/Part/com.miHoYo.GenshinImpact.z01
[GD12_3.3_z02]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/Part/com.miHoYo.GenshinImpact.z02
[GD12_3.3_z03]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/Part/com.miHoYo.GenshinImpact.z03
[GD12_3.3_z04]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/Part/com.miHoYo.GenshinImpact.z04
[GD12_3.3_z05]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/Part/com.miHoYo.GenshinImpact.z05
[GD12_3.3_zip]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.3.0/Global/Part/com.miHoYo.GenshinImpact.zip

<!-- 3.2 GIO Audio -->
[Terabox_Japanese_Voice_3.2]: https://terabox.com/s/1WuhsvW4WXFFtKAlA4EQB2w

<!-- 3.2 GIO GD1-2 -->
[GD12_3.2_z01]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z01
[GD12_3.2_z02]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z02
[GD12_3.2_z03]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z03
[GD12_3.2_z04]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z04
[GD12_3.2_zip]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.zip

<!-- 3.2 GIO GD3 -->
[GD3_3.2_z01]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z01
[GD3_3.2_z02]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z02
[GD3_3.2_z03]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z03
[GD3_3.2_z04]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z04
[GD3_3.2_zip]: https://file2.yuuki.me/GD3/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.zip

<!-- 3.2 GIO OD1 -->
[OD1_3.2_z01]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z01
[OD1_3.2_z02]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z02
[OD1_3.2_z03]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z03
[OD1_3.2_z04]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.z04
[OD1_3.2_zip]: https://file2.yuuki.me/OD1/Project/GenshinImpact/Data/Android/3.2.0/Global/Part/com.miHoYo.ys.x.zip

<!-- 3.2 GIO Terabox -->
[Terabox_3.2_z01]: https://teraboxapp.com/s/1hh9zal0qcafXjz1WJqI0nA
