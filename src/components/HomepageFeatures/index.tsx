import React, { useEffect, useState } from 'react';
import { Container, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import './styles.css'

export interface Root {
    data: Daum[]
    message: string
    retcode: number
    cache: number
}

export interface Daum {
    name: string
    id: string
    server: Server
}

export interface Server {
    online: boolean
    stats: Stats
    game: number
    version: string[]
    privacy: number
    engine: number
    monitor: Monitor
    cpu: Cpu
    ram: Ram
    startup: number
    commit: string
}

export interface Stats {
    player: number
    platform?: Platform
    sub?: Sub
}

export interface Platform {
    ANDROID: number
    CLOUD_ANDROID: number
    CLOUD_IOS: number
    CLOUD_MAC: number
    CLOUD_PC: number
    CLOUD_THIRD_PARTY_MOBILE: number
    CLOUD_THIRD_PARTY_PC: number
    CLOUD_TV: number
    CLOUD_WEB: number
    EDITOR: number
    IOS: number
    PC: number
    PS4: number
    PS5: number
    SERVER: number
}

export interface Sub {
    "1001.5.1.1": number
}

export interface Monitor {
    name: string
    service: string
    type: number
    max: Max
}

export interface Max {
    autorestart: boolean
    ram: number
    cpu: number
}

export interface Cpu {
    usage: number
    core: Core
    usage_raw?: number
}

export interface Core {
    limit: number
    total: number
}

export interface Ram {
    currently: number
    total: number
    usage: number
    limit: number
}

const fetchServerData = async () => {
    const response = await fetch('https://ps.yuuki.me/api/server');
    const output: Root = await response.json();
    const server = output.data.flatMap((Daum) => Daum.server);
    const versionList = server.flatMap((getVer) => getVer.version.filter(str => str.trim() !== ''));
    const name = output.data.flatMap((Daum) => Daum.name);
    return { versionList, name };
}

const App: React.FC = () => {
    const [listVersion, setListVersion] = useState<string[]>();
    const [listRegion, setListRegion] = useState<string[]>();
    const [isError, setIsError] = useState<boolean>(false);

    useEffect(() => {
        fetchServerData().then(({ versionList, name }) => {
            setListVersion(versionList);
            setListRegion(name);
        }).catch(() => setIsError(true));
    }, []);

    return (
        <Container className='container-homepage'>
            <br />
            <Link to='/docs/intro#genshin-impact-support' className='link-homepage'>
                <Card className='card-homepage'>
                    <Card.Title>
                        Support version
                    </Card.Title>
                    <Card.Body>
                        <Card.Text>
                            <>
                                {listVersion &&
                                    listVersion
                                        .filter((version, index, self) => self.indexOf(version) === index)
                                        .map((uniqueVersion, index, array) => (
                                            <>
                                                {uniqueVersion}
                                                {index < array.length - 1 && ', '}
                                            </>
                                        ))
                                }
                                {!listVersion && !isError && <>Please wait...</>}
                                {isError && <>Error while getting version from API</>
                                }
                            </>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Link>
            <a href='https://ps.yuuki.me/game/genshin-impact' className='link-homepage'>
                <Card className='card-homepage'>
                    <Card.Title>
                        Regional Servers
                    </Card.Title>
                    <Card.Body>
                        <Card.Text>
                            <>
                                {listRegion &&
                                    [...new Set(listRegion.map((name) => name.split(' ')[0]))].join(', ')}
                                {!listRegion && !isError && <>Please wait...</>}
                                {isError && <>Error while getting region from API</>}
                            </>

                        </Card.Text>
                    </Card.Body>
                </Card>
            </a>
            <Link to='/docs/category/command-list' className='link-homepage'>
                <Card className='card-homepage'>
                    <Card.Img src={require('@site/static/homepage/command-list.png').default} className='image' />
                    <Card.Body>
                        <Card.Title className='card-title'>
                            Command List
                        </Card.Title>
                        <Card.Text>
                            Command list for GIO and GC with description how the command works and examples.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Link>
            <a href='https://gm.elaxan.com' className='link-homepage'>
                <Card className='card-homepage'>
                    <Card.Img src={require('@site/static/homepage/gm-handbook.png').default} className='image' />
                    <Card.Body>
                        <Card.Title className='card-title'>
                            GM Handbook
                        </Card.Title>
                        <Card.Text>
                            List of IDs for GC and GIO contains IDs for Avatars, Quests, Items, Weapons, Monsters, etc.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </a>
        </Container>
    );
};

export default App