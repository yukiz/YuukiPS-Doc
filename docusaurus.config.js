// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

// Fix error with new update of prism-react-renderer
const lightCodeTheme = require("prism-react-renderer").themes.github;
const darkCodeTheme = require("prism-react-renderer").themes.vsDark;

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: "YuukiPS",
    tagline: "Official document from YuukiPS for Tutorials, Faqs, and more",
    url: "https://doc.yuuki.me",
    baseUrl: "/",
    onBrokenLinks: "warn",
    onBrokenMarkdownLinks: "warn",
    favicon: "img/logo.png",

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: "yukiz", // Usually your GitHub org/user name.
    projectName: "YuukiPS-Doc", // Usually your repo name.

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: "en",
        locales: ["en", "id"],
        localeConfigs: {
            en: {
                label: "English",
            },
            id: {
                label: "Indonesia",
            },
        },
    },
    plugins: [
        require.resolve("docusaurus-lunr-search"),
    ],
    presets: [
        [
            "classic",
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                blog: false, // Optional: disable the blog plugin
                docs: {
                    lastVersion: "current",
                    versions: {
                        current: {
                            label: "dev",
                            path: "",
                        },
                    },
                    sidebarPath: require.resolve("./sidebars.js"),
                },
                theme: {
                    customCss: require.resolve("./src/css/custom.css"),
                },
            }),
        ],
    ],

    themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
        navbar: {
            title: "YuukiPS",
            logo: {
                alt: "YuukiPS Logo",
                src: "img/logo.png",
            },
            items: [
                {
                    type: "localeDropdown",
                    position: "right",
                },
                {
                    type: "docsVersionDropdown",
                    position: "right",
                },
            ],
        },
        footer: {
            style: "dark",
            links: [
                {
                    title: "Community",
                    items: [
                        {
                            label: "Discord Server",
                            href: "https://discord.gg/yuukips",
                        },
                        {
                            label: "GitHub",
                            href: "https://github.com/YuukiPS",
                        },
                        {
                            label: "GitLab",
                            href: "https://gitlab.com/YuukiPS",
                        },
                        {
                            label: "GitLab (Doc)",
                            href: "https://gitlab.com/yukiz/YuukiPS-Doc",
                        },
                    ],
                },
                {
                    title: "Server",
                    items: [
                        {
                            label: "Status",
                            href: "https://stats.uptimerobot.com/L8Nx6FL4V",
                        },
                        {
                            label: "Stats",
                            href: "https://ps.yuuki.me/game/genshin-impact",
                        },
                    ],
                },
                {
                    title: "Sponsors",
                    items: [
                        {
                            label: "Ko-fi",
                            href: "https://ko-fi.com/yuukiz",
                        },
                        {
                            label: "Saweria",
                            href: "https://saweria.co/yuukiz",
                        },
                    ],
                },
            ],
            // eslint-disable-next-line max-len
            copyright: `Copyright © 2023 YuukiPS<br>Powered by Vercel & Docusaurus.`,
        },
        prism: {
            theme: lightCodeTheme,
            darkTheme: darkCodeTheme,
        },
        injectHtmlTags() {
            return {
                headTags: [
                    {
                        tagName: "script",
                        innerHTML: `
              var _Hasync= _Hasync|| [];
              _Hasync.push(['Histats.start', '1,4767529,4,0,0,0,00010000']);
              _Hasync.push(['Histats.fasi', '1']);
              _Hasync.push(['Histats.track_hits', '']);
              (function() {
                var hs = document.createElement('script');
                hs.type = 'text/javascript'; hs.async = true;
                hs.src = ('//s10.histats.com/js15_as.js');
                (document.getElementsByTagName('head')[0] ||
                document.getElementsByTagName('body')[0]).appendChild(hs);
              })();
              `,
                    },
                ],
            };
        },
    }),
};

module.exports = config;
